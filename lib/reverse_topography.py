import random
import math


def rad2deg(angle: float):
    return angle * 180 / math.pi

def rad2grad(angle: float):
    return angle * 200 / math.pi

def deg2rad(angle: float):
    return angle * math.pi / 180

def deg2grad(angle: float):
    return angle * 200 / 180

def grad2pi(angle: float):
    return angle * math.pi / 200

def grad2deg(angle: float):
    return angle * 180 / 200

def calc_distances(avgDistance: float, o_listSize: int, minVariance: float, maxVariance: float, roundAt: float = 3):
    dstSum = avgDistance * o_listSize
    dstList = []
    for __index__ in range(o_listSize-1):
        dstTemp = avgDistance
        if __index__ % 2 == 0:
            dstTemp += random.uniform(minVariance, maxVariance)
        else:
            dstTemp -= random.uniform(minVariance, maxVariance)
        dstList.append(dstTemp.__round__(roundAt))
        dstSum -= dstTemp
    dstList.append(dstSum.__round__(roundAt))
    return dstList

def calc_slope(dstHorizontal: float, variance: float):
    slopeDistance = dstHorizontal + random.uniform(0.000, variance)
    angleRad = math.asin(dstHorizontal/slopeDistance)
    return slopeDistance.__round__(3), rad2grad(angleRad).__round__(4)

def generate_vertical_angles(avgAngle: float, variance):
    vFirst = avgAngle + random.uniform(-variance, variance)
    vSecond = 400 + vFirst - 2*avgAngle
    return vFirst.__round__(4), vSecond.__round__(4)


if __name__ =="__main__":
    finalDistance = 36.803
    numberOfMeasurements = 4

    distances = calc_distances(
        avgDistance=finalDistance,
        o_listSize=numberOfMeasurements,
        minVariance=0.001,
        maxVariance=0.004
    )

    slopes = []
    vAngles = []
    vFirstAngles = []
    vSecondAngles = []
    for dst in distances:
        slopeDist, vAngle = calc_slope(dst, 0.015)
        slopes.append(slopeDist)
        vAngles.append(vAngle)

        firstAngle, secondAngle = generate_vertical_angles(vAngle, 0.010)
        vFirstAngles.append(firstAngle)
        vSecondAngles.append(secondAngle)

    print("| V Angle I |  V Angle II | V Angle Avg | Slope Distance | Horizontal Distance | ")
    for __index__ in range(numberOfMeasurements):
        print(f"| {vFirstAngles[__index__]} | {vSecondAngles[__index__]} | {vAngles[__index__]} | {slopes[__index__]} | {distances[__index__]}")
